package main

import (
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
)

func main() {
	var port string
	flag.StringVar(&port, "p", "8081", "port default is 8081")
	flag.Parse()
	engine := gin.Default()
	engine.POST("/post", PostHandler)
	engine.GET("/get", GetHandler)
	//engine.GET("/no-parameters", NoParameters)
	//engine.GET("/dynamic-route/:name", DynamicRoute)
	//engine.GET("/query-parameters", QueryParameters)

	err := engine.Run(":" + port) // listen and serve on 0.0.0.0:8081
	if err != nil {
		panic(fmt.Sprintf("start fial %v", err))
	}
}
