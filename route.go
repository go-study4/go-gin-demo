package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
)

// NoParameters 无参数
var NoParameters = func(context *gin.Context) {
	context.String(http.StatusOK, "Hello, Golang")
}

// DynamicRoute 动态路由
var DynamicRoute = func(context *gin.Context) {
	name := context.Param("name")
	context.String(http.StatusOK, "Hello, "+name)
}

// QueryParameters Query参数
var QueryParameters = func(context *gin.Context) {
	name := context.Query("name")
	role := context.DefaultQuery("role", "teacher")
	context.String(http.StatusOK, fmt.Sprintf("Hello, %s role = %s", name, role))
}

var GetHandler = func(context *gin.Context) {
	result := make(map[string]any)
	// head
	header := context.Request.Header
	result["head"] = header

	// QueryParameters
	uri := context.Request.RequestURI
	result["uri"] = uri

	context.PureJSON(200, result)
}

var PostHandler = func(context *gin.Context) {
	result := make(map[string]any)

	// head
	header := context.Request.Header
	result["head"] = header

	// body
	body, _ := io.ReadAll(context.Request.Body)
	bodyMap := make(map[string]any)
	bodySlice := make([]any, 0)
	err := json.Unmarshal([]byte(body), &bodyMap)
	if err != nil {
		json.Unmarshal([]byte(body), &bodySlice)
		result["body"] = bodySlice
	} else {
		result["body"] = bodyMap
	}

	// QueryParameters
	uri := context.Request.RequestURI
	result["uri"] = uri
	context.PureJSON(200, result)
}
